# $Id$
# Maintainer (Arch): Jelle van der Waa <jelle@vdwaa.nl>
# Maintainer (Arch): Eli Schwartz <eschwartz@archlinux.org>
# Contributor (Arch): Daniel Wallace <danielwallace at gtmanfred dot com>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Petrov Roman <nwhisper@gmail.com>
# Contributor (Arch): Andrea Fagiani <andfagiani _at_ gmail dot com>
# Contributor (Arch): Larry Hajali <larryhaja@gmail.com>
# Maintainer: Andreas Grapentin <andreas@grapentin.org>
# Contributor: David P.<megver83@parabola.nu>
# Contributor: Jesús E.<heckyel@parabola.nu>
# Contributor: Isaac David <isacdaavid@at@isacdaavid@dot@info>

# parabola changes and rationale:
#  - removed decrompession engine for rar archives

pkgname=calibre
pkgver=3.28.0
pkgrel=2
pkgrel+=.parabola2
pkgdesc="Ebook management application"
pkgdesc+=", without nonfree decompression engine for RAR archives"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://calibre-ebook.com/"
license=('GPL3')
depends=('python2-six' 'python2-dateutil' 'python2-cssutils' 'python2-dukpy'
         'python2-mechanize' 'podofo' 'libwmf'
         'chmlib' 'python2-lxml' 'libusbx' 'python2-html5-parser'
         'python2-pillow' 'shared-mime-info' 'python2-dnspython' 'python2-msgpack'
         'python2-pyqt5' 'python2-psutil' 'icu' 'libmtp' 'python2-dbus'
         'python2-netifaces' 'python2-cssselect' 'python2-apsw' 'qt5-webkit'
         'qt5-svg' 'python2-regex' 'python2-pygments' 'mtdev'
         'desktop-file-utils' 'gtk-update-icon-cache' 'optipng' 'udisks2')
makedepends=('qt5-x11extras' 'sip' 'xdg-utils')
optdepends=('ipython2: to use calibre-debug'
            'poppler: required for converting pdf to html'
)
source=("https://download.calibre-ebook.com/${pkgver}/calibre-${pkgver}.tar.xz"
        "https://calibre-ebook.com/signatures/${pkgname}-${pkgver}.tar.xz.sig"
        'libre.patch')
sha256sums=('241050cb89e5a70d3195cfd91ccf406919ac0bfb437e34b538c954c306d87b2c'
            'SKIP'
            'f418669d2e32e917e3292350150015ed6dd13f8c2dd82ac5f1c5d20c112dd2e8')
validpgpkeys=('3CE1780F78DD88DF45194FD706BC317B515ACE7C') # Kovid Goyal (New longer key) <kovid@kovidgoyal.net>

prepare() {
  cd "${pkgname}-${pkgver}"

  # Remove unneeded files
  rm -rf resources/calibre-portable.*

  # fix freedom issues
  rm -v imgsrc/mimetypes/rar.svg
  rm -v src/calibre/ebooks/metadata/rar.py
  rm -v src/calibre/utils/unrar.py
  rm -v resources/images/mimetypes/{cbr,rar}.png
  patch -fNp1 -i "$srcdir/libre.patch"

  # Desktop integration (e.g. enforce arch defaults)
  sed -e "/self.create_uninstaller()/,/os.rmdir(config_dir)/d" \
      -e "/cc(\['xdg-desktop-menu', 'forceupdate'\])/d" \
      -e "/cc(\['xdg-mime', 'install', MIME\])/d" \
      -e "s/'ctc-posml'/'text' not in mt and 'pdf' not in mt and 'xhtml'/" \
      -e "s/^Name=calibre/Name=Calibre/g" \
      -i  src/calibre/linux.py
}

build() {
  cd "${pkgname}-${pkgver}"

  LANG='en_US.UTF-8' python2 setup.py build
  LANG='en_US.UTF-8' python2 setup.py gui
}

package() {
  cd "${pkgname}-${pkgver}"

  install -d "${pkgdir}/usr/share/zsh/site-functions" \
             "${pkgdir}"/usr/share/{applications,desktop-directories,icons/hicolor}

  install -Dm644 resources/calibre-mimetypes.xml \
    "${pkgdir}/usr/share/mime/packages/calibre-mimetypes.xml"

  XDG_DATA_DIRS="${pkgdir}/usr/share" LANG='en_US.UTF-8' \
    python2 setup.py install --staging-root="${pkgdir}/usr" --prefix=/usr

  cp -a man-pages/ "${pkgdir}/usr/share/man"

  # Compiling bytecode FS#33392
  python2 -m compileall "${pkgdir}/usr/lib/calibre/"
  python2 -O -m compileall "${pkgdir}/usr/lib/calibre/"
}
