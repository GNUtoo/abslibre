# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_libname=xmlgraphics-commons
pkgname=java-${_libname}
pkgver=2.1
pkgrel=1.parabola1
pkgdesc="Common components between FOP and Batik."
arch=('any')
url="http://xmlgraphics.apache.org/commons/"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-commons-io' 'java-commons-logging' 'java-environment' 'jh')
source=("https://archive.apache.org/dist/xmlgraphics/commons/source/${_libname}-${pkgver}-src.tar.gz")
md5sums=('57c14bf1b8aef8a36f00e943215ffd21')

prepare() {
  cd "${srcdir}/${_libname}-${pkgver}"
  rm -v $(find . -name \*.jar)
  mkdir -p build/classes
  sed -i "s/@version@/${pkgver}/g" xmlgraphics-commons-pom-template.pom 
}

build() {
  cd "${srcdir}/${_libname}-${pkgver}"

  CLASSPATH="/usr/share/java/commons-io.jar:/usr/share/java/commons-logging.jar"
  javac -encoding UTF-8 -classpath $CLASSPATH -d "build/classes" \
    $(find "src/java" -name \*.java)
  javadoc -encoding UTF-8 -classpath $CLASSPATH -d "build/javadoc" \
    -sourcepath "src/java" -subpackages org

  jar -cvf "${_libname}.jar" -C "build/classes" .
}

package() {
  cd "${srcdir}/${_libname}-${pkgver}"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "examples/java" "${pkgdir}/usr/share/doc/${pkgname}/examples"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "org.apache.xmlgraphics" ${_libname} ${pkgver} \
    "${srcdir}/${_libname}-${pkgver}/${_libname}-pom-template.pom" \
    "${_libname}.jar" \
    "${_libname}-${pkgver}.jar"
  ln -s "/usr/share/java/${_libname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${_libname}.jar"

  install -d "${pkgdir}/usr/share/java/${_libname}"
  ln -s "/usr/share/java/${_libname}.jar" \
    "${pkgdir}/usr/share/java/${_libname}/${_libname}.jar"
}
