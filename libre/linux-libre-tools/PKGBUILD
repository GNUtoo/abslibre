# Maintainer (Arch): Sébastien Luttringer
# Maintainer: David P. <megver83@parabola.nu>
# Contributor: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# parabola changes and rationale (adapted from core/linux-tools):
#  - changed upstream source url to linux-libre
#  - removed hyperV support

pkgbase=linux-libre-tools
pkgname=(
  'cgroup_event_listener'
  'cpupower'
  'libtraceevent'
  'linux-libre-tools-meta'
  'perf'
  'tmon'
  'usbip'
)
case "$CARCH" in
  x86_64|i686) pkgname+=('turbostat' 'x86_energy_perf_policy') ;;
  armv7h)      pkgname+=('gpio-utils' 'iio-utils') ;;
esac

pkgver=4.18_gnu
pkgrel=1
license=('GPL2')
arch=('x86_64')
arch+=('i686' 'armv7h')
url='https://linux-libre.fsfla.org/'
options=('!strip')
makedepends=('git')
# split packages need all package dependencies set manually in makedepends
# kernel source deps
makedepends+=('asciidoc' 'xmlto')
# perf deps
makedepends+=('perl' 'python2' 'slang' 'elfutils' 'libunwind' 'audit' 'gtk2')
makedepends_x86_64=('numactl')
makedepends_i686=('numactl')
# cpupower deps
makedepends+=('pciutils')
# usbip deps
makedepends+=('glib2' 'sysfsutils' 'udev')
# tmon deps
makedepends+=('ncurses')
groups=("$pkgbase")
source=("https://linux-libre.fsfla.org/pub/linux-libre/releases/${pkgver//_/-}/linux-libre-${pkgver//_/-}.tar.xz"{,.sign}
#        "https://linux-libre.fsfla.org/pub/linux-libre/releases/${pkgver%.*}-gnu/patch-${pkgver%.*}-gnu-${pkgver//_/-}.xz"{,.sign}
        'cpupower.default'
        'cpupower.systemd'
        'cpupower.service'
        'usbipd.service')
sha256sums=('55ddf20702311cf3e32127135a38cb272313679962e8a798c108b01cadaeca4f'
            'SKIP'
            '4fa509949d6863d001075fa3e8671eff2599c046d20c98bb4a70778595cd1c3f'
            'd2e8e5e8b22c6089a91f573aa1c59e442a1f3b67a2c9f047abe3b57d3d6558cc'
            'fa2560630576464739ede14c9292249f4007f36a684bc378add174fc88394550'
            '2e187734d8aec58a3046d79883510d779aa93fb3ab20bd3132c1a607ebe5498f')
validpgpkeys=('474402C8C582DAFBE389C427BCB7CF877E7D47A7') # Alexandre Oliva

prepare() {
  cd linux-${pkgver%_*}

  # apply patch from the source array (should be a pacman feature)
  local filename
  for filename in "${source[@]}"; do
    filename="${filename##*/}"
    if [[ "$filename" =~ \.patch$ ]]; then
      msg2 "Applying patch $filename"
      patch -p1 -N -i "$srcdir/$filename"
    elif [[ "$filename" =~ ^patch- ]]; then
      msg2 "Applying linux $filename"
      patch -p1 -N -i "$srcdir/${filename%.*}"
    fi
  done
}

build() {
  msg2 'libtraceevent'
  pushd linux-${pkgver%_*}/tools/lib/traceevent
  make
  popd

  msg2 'perf'
  pushd linux-${pkgver%_*}/tools/perf
  make -f Makefile.perf \
    prefix=/usr \
    lib=lib/perf \
    perfexecdir=lib/perf \
    NO_SDT=1 \
    PYTHON=python2 \
    PYTHON_CONFIG=python2-config \
    PERF_VERSION=$pkgver-$pkgrel \
    DESTDIR="$pkgdir" \
    all man
  popd

  msg2 'cpupower'
  pushd linux-${pkgver%_*}/tools/power/cpupower
  make VERSION=$pkgver-$pkgrel
  popd

if [[ "$CARCH" == x86_64 ]] || [[ "$CARCH" == i686 ]]; then
  msg2 'x86_energy_perf_policy'
  pushd linux-${pkgver%_*}/tools/power/x86/x86_energy_perf_policy
  make
  popd
fi

  msg2 'usbip'
  pushd linux-${pkgver%_*}/tools/usb/usbip
  ./autogen.sh
  ./configure --prefix=/usr --sbindir=/usr/bin
  make
  popd

  msg2 'tmon'
  pushd linux-${pkgver%_*}/tools/thermal/tmon
  make
  popd

  msg2 'cgroup_event_listener'
  pushd linux-${pkgver%_*}/tools/cgroup
  make
  popd

if [[ "$CARCH" == x86_64 ]] || [[ "$CARCH" == i686 ]]; then
  msg2 'turbostat'
  pushd linux-${pkgver%_*}/tools/power/x86/turbostat
  make
fi

if [[ "$CARCH" == armv7h ]]; then
  msg2 'gpio'
  pushd linux-${pkgver%_*}/tools/gpio
  make -j1
  popd

  msg2 'iio'
  pushd linux-${pkgver%_*}/tools/iio
  make -j1
  popd
fi
}

package_linux-libre-tools-meta() {
  pkgdesc='GNU/Linux-libre kernel tools meta package'
  groups=()
  depends=(
    'cgroup_event_listener'
    'cpupower'
    'libtraceevent'
    'perf'
    'tmon'
    'usbip'
  )
  conflicts=(
    'acpidump'
  )
  depends_x86_64=('turbostat' 'x86_energy_perf_policy')
  depends_i686=('turbostat' 'x86_energy_perf_policy')
  depends_armv7h=('gpio-utils' 'iio-utils')
  replaces=(linux-tools-meta)
  conflicts+=(linux-tools-meta)
  provides=(linux-tools-meta)
}

package_libtraceevent() {
  pkgdesc='GNU/Linux-libre kernel trace event library'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/lib/traceevent
  install -dm 755 "$pkgdir/usr/lib"
  install -m 644 libtraceevent.so "$pkgdir/usr/lib"
}

package_perf() {
  pkgdesc='GNU/Linux-libre kernel performance auditing tool'
  depends=('perl' 'python2' 'slang' 'elfutils' 'libunwind' 'binutils'
           'audit')
  depends_x86_64=('numactl')
  depends_i686=('numactl')
  optdepends=('gtk2: support GTK2 browser for perf report')

  cd linux-${pkgver%_*}/tools/perf
  make -f Makefile.perf \
    prefix=/usr \
    lib=lib/perf \
    perfexecdir=lib/perf \
    NO_SDT=1 \
    PYTHON=python2 \
    PYTHON_CONFIG=python2-config \
    PERF_VERSION=$pkgver-$pkgrel \
    DESTDIR="$pkgdir" \
    install install-man
  cd "$pkgdir"
  # add linker search path
  mkdir "$pkgdir/etc/ld.so.conf.d"
  echo '/usr/lib/perf' > "$pkgdir/etc/ld.so.conf.d/$pkgname.conf"
  # move completion in new directory
  install -Dm644 etc/bash_completion.d/perf usr/share/bash-completion/completions/perf
  rm -r etc/bash_completion.d
  # no exec on usr/share
  find usr/share -type f -exec chmod a-x {} \;
}

package_cpupower() {
  pkgdesc='GNU/Linux-libre kernel tool to examine and tune power saving related features of your processor'
  backup=('etc/default/cpupower')
  depends=('bash' 'pciutils')
  conflicts=('cpufrequtils')
  replaces=('cpufrequtils')
  install=cpupower.install

  pushd linux-${pkgver%_*}/tools/power/cpupower
  make \
    DESTDIR="$pkgdir" \
    sbindir='/usr/bin' \
    libdir='/usr/lib' \
    mandir='/usr/share/man' \
    docdir='/usr/share/doc/cpupower' \
    install install-man
  popd
  # install startup scripts
  install -Dm 644 $pkgname.default "$pkgdir/etc/default/$pkgname"
  install -Dm 644 $pkgname.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -Dm 755 $pkgname.systemd "$pkgdir/usr/lib/systemd/scripts/$pkgname"
}

package_x86_energy_perf_policy() {
  pkgdesc='Read or write MSR_IA32_ENERGY_PERF_BIAS'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/power/x86/x86_energy_perf_policy
  install -Dm 755 x86_energy_perf_policy "$pkgdir/usr/bin/x86_energy_perf_policy"
  install -Dm 644 x86_energy_perf_policy.8 "$pkgdir/usr/share/man/man8/x86_energy_perf_policy.8"
}

package_usbip() {
  pkgdesc='An USB device sharing system over IP network'
  depends=('glib2' 'sysfsutils' 'libsystemd')

  pushd linux-${pkgver%_*}/tools/usb/usbip
  make install DESTDIR="$pkgdir"
  popd
  # module loading
  install -Dm 644 /dev/null "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
  printf 'usbip-core\nusbip-host\n' > "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
  # systemd
  install -Dm 644 usbipd.service "$pkgdir/usr/lib/systemd/system/usbipd.service"
}

package_tmon() {
  pkgdesc='Monitoring and Testing Tool for GNU/Linux-libre kernel thermal subsystem'
  depends=('glibc' 'ncurses')

  cd linux-${pkgver%_*}/tools/thermal/tmon
  make install INSTALL_ROOT="$pkgdir"
}

package_cgroup_event_listener() {
  pkgdesc='Simple listener of cgroup events'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/cgroup
  install -Dm755 cgroup_event_listener "$pkgdir/usr/bin/cgroup_event_listener"
}

package_turbostat() {
  pkgdesc='Report processor frequency and idle statistics'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/power/x86/turbostat
  make install DESTDIR="$pkgdir"
}

package_gpio-utils() {
  pkgdesc='GPIO character device utilities'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/gpio
  make install DESTDIR="$pkgdir"
}

package_iio-utils() {
  pkgdesc='Industrial IO utilities'
  depends=('glibc')

  cd linux-${pkgver%_*}/tools/iio
  make install DESTDIR="$pkgdir"
}

# vim:set ts=2 sw=2 et:
