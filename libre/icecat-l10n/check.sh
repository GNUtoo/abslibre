#!/bin/bash
# helper script to check the local language list against upstream

# extract pkgbase from pkgbuild
pkgbase="$(bash -c "source PKGBUILD && echo \"\$pkgbase\"")"

# extract language list from pkgbuild
bash -c "source PKGBUILD && printf '%s\n' \"\${_languages[@]}\" | cut -d ' ' -f1 | sort" \
  > $pkgbase-l10n.local

# load language list from upstream
url="$(bash -c "source PKGBUILD && echo \"\$_url\"")/"
curl -sL $url | \
  grep '\.xpi"' | cut -d'"' -f8 | rev | cut -d'/' -f1 | cut -d'.' -f3 | rev | sort \
  > $pkgbase-l10n.remote

diff -rupN $pkgbase-l10n.local $pkgbase-l10n.remote
rm -f $pkgbase-l10n.{local,remote}
