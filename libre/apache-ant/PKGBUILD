# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (Arch): Guillaume ALAUX <guillaume@archlinux.org>
# Contributor (Arch): Andrew Wright <andreww@photism.org>
# Contributor (Arch): Paul Mattal <paul@archlinux.org>

# FIXME: this seems to also be a replacement and provider of
#        apache-ant-doc. a split package should be made for it, or it
#        should be explicitly conflicted at the very least.

pkgname=apache-ant
pkgver=1.9.7
pkgdesc='A java-based build tool'
pkgrel=1.parabola1
arch=('any')
url='http://ant.apache.org/'
license=('APACHE' 'custom:BSD3')
depends=('java-environment')
makedepends=('jh')
install=apache-ant.install
source=(https://www.apache.org/dist/ant/source/${pkgname}-${pkgver}-src.tar.bz2
        apache-ant.sh
        apache-ant.csh
        apache-ant.install
        bin_ant
        ant.conf)
sha256sums=('9e5c36759b81e0b16f2f1747631e81dc86fe904e45b413a6ca15d10b3282ab0b'
            '182b9212610790966d3a018d3cbab2e5327dd9fd11dc15dd1f377853216f5718'
            '919a3ab1acae1d0d190fe6e0034b00975caab40e55359a720bfccd098fe2d995'
            '3de451d0f963ba96e83f4e068a8325e3d4fc42b1e5c4818e9d7962ed4f5393b3'
            '29b443ae3af7e246b4ff0b5ec9f023ae95992db1a010a362e70ca7a65a6df461'
            'b86ce60f61cbd85a9faa61d698b0fc4607f1ff375cd15673aee57f053012eacb')

# This list is adapted from https://ant.apache.org/manual/install.html#librarydependencies
# The format is:
#   jarname:description:pkgname:jarpath
_library_dependencies=(
  jakarta-oro-2.0.8.jar:'<ftp task>':jakarta-oro:/usr/share/java/jakarta-oro.jar
  junit.jar:'<junit> task':junit:/usr/share/java/junit.jar
  xalan.jar:'<junitreport> task':xalan-java:/usr/share/java/xalan.jar
  antlr.jar:'<antlr> task':java-antlr2:/usr/share/java/antlr2.jar
  bsf.jar:'<script> task':java-commons-bsf2:/usr/share/java/bsf2.jar
  groovy-all.jar:'Groovy with <script> and <scriptdef> tasks':groovy:/usr/share/groovy/embeddable/groovy-all.jar
  js.jar:'Javascript with <script> task':java-rhino:/usr/share/java/js.jar
  jython.jar:'Python with <script> task':jython:/opt/jython/jython.jar
  beanshell.jar:'BeanShell with <script> task':'beanshell2':/usr/share/java/bsh.jar
  jruby.jar:'Ruby with <script> task':jruby:/opt/jruby/lib/jruby.jar
  commons-logging.jar:'CommonsLoggingListener':java-commons-logging:/usr/share/java/commons-logging.jar
  log4j.jar:'Log4jListener':'log4j-1.2':'/usr/share/java/log4j-1.2.jar'
  commons-net.jar:'<ftp>, <rexec> and <telnet> tasks':'java-commons-net1>=1.4.0':/usr/share/java/commons-net.jar
  bcel.jar:"'classfileset' data type, JavaClassHelper for ClassConstants filter reader":java-bcel:/usr/share/java/bcel.jar
  mail.jar:'<mimemail> task, mime encoding with <mail>':java-gnumail:/usr/share/java/gnumail.jar
  activation.jar:'<mimemail> task, mime encoding with <mail>':java-activation-gnu:/usr/share/java/activation.jar
  jdepend.jar:'<jdepend> task':java-jdepend:/usr/share/java/jdepend.jar
  resolver.jar:"external files for 'xmlcatalog' datatype":java-resolver:/usr/share/java/resolver.jar
  jsch.jar:'<sshexec> and <scp> tasks':'java-jsch>=0.1.42':/usr/share/java/jsch.jar
  hamcrest-core-1.3.jar:'java matcher objects library':java-hamcrest:/usr/share/java/hamcrest-core.jar
)

for _dep in "${_library_dependencies[@]}"; do
  IFS=: read _jarname _desc _pkgname _jarpath <<<"$_dep"
  optdepends+=("${_pkgname}: ${_desc}")
  makedepends+=("${_pkgname}")
done
unset _dep _jarname _desc _pkgname _jarpath

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  rm -rv lib/optional/*.jar
  rm -rv src/etc/testcases/taskdefs/conditions/jars/*.jar

  # Symlink to external optional libraries/
  local _dep _jarname _desc _pkgname _jarpath
  for _dep in "${_library_dependencies[@]}"; do
    IFS=: read _jarname _desc _pkgname _jarpath <<<"$_dep"
    ln -sfv "${_jarpath}" lib/optional/${_jarname}
  done
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Build
  export JAVA_HOME=/usr/lib/jvm/default
  sh build.sh dist
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}/${pkgbase}-${pkgver}"
  source "${srcdir}"/${pkgname}.sh # Get the ANT_HOME env var

  ## Install the main distribution
  install -d "${pkgdir}/${ANT_HOME}"

  install -d "${pkgdir}/etc/${pkgname}"
  cp -Rp etc/* "${pkgdir}/etc/${pkgname}"
  rm "${pkgdir}/etc/${pkgname}/ant-bootstrap.jar"
  ln -s /etc/${pkgname} "${pkgdir}${ANT_HOME}/etc"

  install -d "${pkgdir}${ANT_HOME}/bin"
  find bin -type f -a ! -name \*.bat -a ! -name \*.cmd \
    -exec install -m755 {} "${pkgdir}${ANT_HOME}/bin" \;

  install -Dm755 "${srcdir}/bin_ant"  "${pkgdir}/usr/bin/ant"
  install -Dm644 "${srcdir}/ant.conf" "${pkgdir}/etc/apache-ant/ant.conf"

  # Fix python2 path
  sed -i 's|/usr/bin/python|/usr/bin/python2|' "${pkgdir}/${ANT_HOME}/bin/runant.py"

  ## Install profile.d script
  install -d "${pkgdir}/etc/profile.d"
  install -m755 "${srcdir}"/apache-ant.{csh,sh} "${pkgdir}/etc/profile.d/"

  # Symlink to external optional libraries
  install -d "${pkgdir}/usr/share/apache-ant/lib"
  local _dep _jarname _desc _pkgname _jarpath
  for _dep in "${_library_dependencies[@]}"; do
    IFS=: read _jarname _desc _pkgname _jarpath <<<"$_dep"
    ln -svfr "${pkgdir}/${_jarpath}" "${pkgdir}${ANT_HOME}/lib/${_jarname}"
  done

  ## Install Maven artifacts, except 'ant-jai' and 'ant-netrexx'
  cd lib
  rm {ant-jai,ant-netrexx}.{jar,pom}
  install -d "${pkgdir}/usr/share/java/${pkgname}"

  export DESTDIR=${pkgdir}
  for artifact in $(printf '%s\n' *.pom|sed 's/\.pom$//'); do
    if [[ -f "${artifact}.jar" ]]; then
      # This artifact has a jar file
      jh mvn-install "org.apache.ant" ${artifact} ${pkgver} \
        "${artifact}.pom" \
        "${artifact}.jar" \
        "${artifact}.jar"

      # Symlink them to /usr/share/java
      ln -s "/usr/share/java/${artifact}.jar" \
        "${pkgdir}/usr/share/java/${artifact}-${pkgver}.jar"
      ln -s "/usr/share/java/${artifact}.jar" \
        "${pkgdir}/usr/share/java/apache-ant/${artifact}.jar"
      ln -s "/usr/share/java/${artifact}.jar" \
        "${pkgdir}/usr/share/apache-ant/lib/${artifact}.jar"
    else
      # This artifact is just a pom
      jh mvn-install "org.apache.ant" ${artifact} ${pkgver} \
        "${artifact}.pom"
    fi
  done

  install -d "${pkgdir}"/usr/share/{doc,licenses}/${pkgname}

  # Install documentation
  cp "../README" "../WHATSNEW" "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "../manual" "${pkgdir}/usr/share/doc/${pkgname}/html"

  # Install license
  install -Dm644 "../LICENSE" "../NOTICE" "${pkgdir}/usr/share/licenses/${pkgname}"
}
