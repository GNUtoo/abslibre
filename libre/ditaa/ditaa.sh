#!/bin/sh
COMMONS_CLI=/usr/share/java/commons-cli.jar
JERICHO_HTML=/usr/share/java/jericho-html.jar
DITAA=/usr/share/java/ditaa.jar
${JAVA_HOME}/bin/java -cp "$COMMONS_CLI:$JERICHO_HTML:$DITAA" \
  org.stathissideris.ascii2image.core.CommandLineConverter "$@"
