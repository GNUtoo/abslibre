<project name="junit" default="dist" basedir="."
         xmlns:artifact="antlib:org.apache.maven.artifact.ant">
  <tstamp />
  <taskdef resource="net/sf/antcontrib/antcontrib.properties"/>

  <property file="${user.home}/.junit.properties" />
  <property name="src" value="src/main/java" />
  <property name="srcresources" location="src/main/resources" />
  <property name="target" location="target" />
  <property name="bin" location="${target}/main" />
  <property name="version-base" value="4.12" />
  <property name="version" value="${version-base}" />
  <property name="dist" value="junit${version}" />
  <property name="versionfile" value="${src}/junit/runner/Version.java" />
  <property name="testsrc" location="src/test/java" />
  <property name="testsresources" location="src/test/resources" />
  <property name="testbin" location="${target}/test/java" />
  <property name="unjarred" 
            value="**/*.jar, ${testfiles}, doc/**, README.html, .classpath, .project, cpl-v10.html" />

  <property name="binjar" value="junit-${version}.jar" />
  <property name="srcjar" value="junit-${version}-sources.jar" />

  <property name="javadocdir" location="${dist}/javadoc" />
  <property name="hamcrestlib" location="lib/hamcrest-core-1.3.jar" />

  <target name="init">
    <tstamp/>
  </target>

  <target name="versiontag" depends="init">
    <filter token="version" value="${version}" />
  
    <copy 
        file="${versionfile}.template" 
        tofile="${versionfile}" 
        filtering="on"
        overwrite="true"
        />
  </target>

  <target name="clean">
    <!-- If two builds are made within a minute -->
    <delete dir="${dist}" quiet="true" />
    <!-- Delete all previous temporary build artifacts -->
    <delete dir="${target}" quiet="true" />
  </target>

  <macrodef name="junit_compilation">
    <attribute name="srcdir"/>
    <attribute name="destdir"/>
    <attribute name="classpath"/>
    <sequential>
      <mkdir dir="@{destdir}"/>
      <javac 
          srcdir="@{srcdir}"
          destdir="@{destdir}"
          debug="on"
          classpath="@{classpath}"
          includeantruntime="false"
          source="1.5"
          target="1.5"
          >
        <compilerarg value="-Xlint:unchecked" />
      </javac>
    </sequential>
  </macrodef>
  
  <target name="build" depends="versiontag">
    <junit_compilation srcdir="${src}" destdir="${bin}" classpath="${hamcrestlib}"/>
    <junit_compilation srcdir="${testsrc}" destdir="${testbin}" classpath="${hamcrestlib};${bin}"/>
  </target>

  <target name="jars" depends="build">
    <mkdir dir="${dist}" />
    <jar 
        jarfile="${dist}/${srcjar}"
        basedir="${src}"
        excludes="${unjarred}, **/*.class"
        />
    <jar 
        jarfile="${dist}/${binjar}"
        basedir="${bin}"
        excludes="${unjarred}, **/*.java, build.xml"
        />
  </target>

  <target name="samples-and-tests">
    <copy todir="${dist}">
      <fileset dir="${testbin}" />
      <fileset dir="${testsrc}" />
    </copy>
  </target>
  
  <target name="javadoc">
    <javadoc destdir="${javadocdir}"
             author="false"
             version="false"
             use="false"
             windowtitle="JUnit API"
             stylesheetfile="src/main/javadoc/stylesheet.css"
             >
      <excludepackage name="junit.*" />
      <excludepackage name="org.junit.internal.*" />
      <excludepackage name="org.junit.experimental.theories.internal.*" />
      
      <sourcepath location="${src}" />
      <link href="http://docs.oracle.com/javase/1.5.0/docs/api/" />

      <classpath>
        <pathelement location="${hamcrestlib}" />
      </classpath>
    </javadoc>
  </target>

  <target name="populate-dist" 
          depends="clean, build, jars, samples-and-tests, javadoc"
          >
    <copy todir="${dist}/doc">
      <fileset dir="doc"/>
    </copy>
    <copy file="README.md" tofile="${dist}/README.md" />
    <copy file="BUILDING" tofile="${dist}/BUILDING" />
    <copy file="epl-v10.html" tofile="${dist}/epl-v10.html" />
    <copy file="build.xml" tofile="${dist}/build.xml" />
  </target>

  <macrodef name="run-dist-tests">
    <!-- Runs the tests against the built jar files -->
    <element name="extra-args" implicit="yes" />
    <sequential>
      <java classname="org.junit.runner.JUnitCore" fork="yes" failonerror="true">
        <extra-args />  
        <arg value="org.junit.tests.AllTests"/>
        <classpath>
          <pathelement location="${dist}" />
          <pathelement location="${dist}/${binjar}" />
          <pathelement location="${hamcrestlib}" />
          <pathelement location="${testsresources}" />
        </classpath>
      </java>    
    </sequential>
  </macrodef>

  <macrodef name="run-local-tests">
    <!-- Runs the tests against the local class files -->
    <sequential>
      <java classname="org.junit.runner.JUnitCore" fork="yes" failonerror="true">
        <arg value="org.junit.tests.AllTests"/>
        <classpath>
          <pathelement location="${bin}" />
          <pathelement location="${testbin}" />
          <pathelement location="${hamcrestlib}" />
          <pathelement location="${testsresources}" />
        </classpath>
      </java>    
    </sequential>
  </macrodef>

  <target name="test" depends="build">
    <run-local-tests />
  </target>

  <target name="dist" depends="populate-dist">
    <run-dist-tests>
      <jvmarg value="-Dignore.this=ignored"/>
    </run-dist-tests>
  </target>

  <target name="profile" depends="populate-dist">
    <run-dist-tests>
      <jvmarg value="-agentlib:hprof=cpu=samples"/>
    </run-dist-tests>
  </target>
</project>
