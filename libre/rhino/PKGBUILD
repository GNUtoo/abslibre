# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgbase=rhino
pkgname=('java-rhino' 'rhino-javadoc')
pkgver=1.7.7.2
_pkgver=${pkgver//\./_}
pkgrel=1
pkgrel+=.par1
arch=('any')
url="http://www.mozilla.org/rhino/"
license=('MPL')
makedepends=('apache-ant' 'jh')
source=(https://github.com/mozilla/${pkgbase}/archive/Rhino${_pkgver}_Release.tar.gz)
sha512sums=('39675c064fc902678e15baec546986a78d6109ab1cbceae5166773607abcec527ae533ea4540ee67501a5ca02a9dcc467267e722af21b93e72fdb2ede1935ad8')

prepare() {
  cd "${srcdir}/${pkgbase}-Rhino${_pkgver}_Release"
  rm -v testsrc/tests/src/jstests.jar
  rm -v testsrc/org/mozilla/javascript/tests/commonjs/module/modules.jar
}

build() {
  cd "${srcdir}/${pkgbase}-Rhino${_pkgver}_Release"
  ant \
    jar \
    javadoc
}

package_java-rhino() {
  pkgdesc="Open-source implementation of JavaScript written entirely in Java - JAR"
  depends=('java-runtime-headless')

  cd ${srcdir}/${pkgbase}-Rhino${_pkgver}_Release

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "org.mozilla" ${pkgbase} ${pkgver} \
    "maven/maven-pom.xml" \
    "build/${pkgbase}${pkgver}/js.jar" \
    "js-${pkgver}.jar"

  ln -s "/usr/share/java/js-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/js.jar"
}

package_rhino-javadoc() {
  pkgdesc="Open-source implementation of JavaScript written entirely in Java - Javadoc"

  cd ${srcdir}/${pkgbase}-Rhino${_pkgver}_Release
  install -m755 -d ${pkgdir}/usr/share/${pkgbase}
  cp -r build/${pkgbase}${pkgver}/javadoc ${pkgdir}/usr/share/${pkgbase}
}
