# $Id: PKGBUILD 212324 2017-02-14 09:21:33Z arodseth $
# Maintainer (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): jlvsimoes <jlvsimoes@oninet.pt>
# Contributor (Arch): kevin <kevin@archlinux.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=abuse
_pkgname=$pkgname-libre
pkgver=0.8
pkgrel=8.parabola2
pkgdesc='Side-scroller action game that pits you against ruthless alien killers, without nonfree claudio support'
arch=('x86_64' 'i686' 'armv7h')
url='http://abuse.zoy.org/'
license=('GPL' 'custom')
replaces=($_pkgname)
conflicts=($_pkgname)
depends=('desktop-file-utils' 'gcc-libs' 'glu' 'libgl' 'sdl_mixer')
makedepends=('gendesk' 'glu' 'mesa-libgl')
mksource=("http://$pkgname.zoy.org/raw-attachment/wiki/download/$pkgname-$pkgver.tar.gz")
source=("https://repo.parabola.nu/other/$_pkgname/$_pkgname-$pkgver.tar.gz"
        "$pkgname.sh"
        "default.${pkgname}rc"
        'default.gamma.lsp'
        'libre.patch')
mksha256sums=('0104db5fd2695c9518583783f7aaa7e5c0355e27c5a803840a05aef97f9d3488')
sha256sums=('8f77e8020bf17b10d8a5269d7d4d9bc3592f961ce352672ce183e1f41efc956b'
            '8dcf3b6b6ac419e5ffabc847166917d70ce43af3092c4d651f473947fff0cc7d'
            '6ea2fa9e65c10bbcab4432f40d0c7284cb3970835c40c5c00ced14b39ce1b00f'
            '13df83fea60918d29d5141cdfbd8d56147c053e88d3701209ae676f46447c2f6'
            '7566a3affb01c64ff4f859bcd549661220847e9c62327e71e1d585c0048f90ea')

mksource() {
  cd "$pkgname-$pkgver"

  # Remove the nonfree claudio addon
  rm -rvf ./data/addon/claudio
}

prepare() {
  cd "$pkgname-$pkgver"

  patch -p1 -i "$srcdir/libre.patch"

  gendesk -f -n --pkgname "$pkgname" --pkgdesc "$pkgdesc"
}

build() {
  cd "$pkgname-$pkgver"

  ./configure \
    --prefix=/usr \
    --with-assetdir="/usr/share/$pkgname/orig" \
    --with-x \
    --enable-debug # Fix for FS#52915
  make
}

package() {
  cd "$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install

  # Desktop shortcut and icon
  install -Dm644 "$pkgname.desktop" \
    "$pkgdir/usr/share/applications/$pkgname.desktop"
  install -Dm644 "doc/$pkgname.png" "$pkgdir/usr/share/pixmaps/$pkgname.png"

  # Default configuration
  mv "$pkgdir/usr/bin/$pkgname" "$pkgdir/usr/bin/$pkgname.elf"
  install -Dm755 "$srcdir/$pkgname.sh" "$pkgdir/usr/bin/$pkgname"
  install -Dm644 "$srcdir/default.${pkgname}rc" \
    "$pkgdir/usr/share/$pkgname/default.${pkgname}rc"
  install -Dm644 "$srcdir/default.gamma.lsp" \
    "$pkgdir/usr/share/$pkgname/default.gamma.lsp"

  # License (for the original source code)
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n '1,/^$/p' src/view.cpp > "$pkgdir/usr/share/licenses/$pkgname/original"
}

# vim:set ts=2 sw=2 et:
