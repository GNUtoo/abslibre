# $Id: PKGBUILD 288091 2017-02-05 19:09:19Z foutrelis $
# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Thayer Williams <thayer@archlinux.org>
# Contributor (Arch): Hugo Doria <hugo@archlinux.org>
# Contributor (Arch): TuxSpirit<tuxspirit@archlinux.fr>  2007/11/17 21:22:36 UTC
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer (Hyperbola): André Silva <emulatorman@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=p7zip-libre
pkgname=p7zip
pkgver=16.02
pkgrel=5.parabola1
pkgdesc="Command-line file archiver with high compression ratio"
arch=('i686' 'x86_64' 'armv7h')
url="http://p7zip.sourceforge.net/"
license=('LGPL')
conflicts=('p7zip-libre')
replaces=('p7zip-libre')
depends=('gcc-libs' 'sh')
makedepends_i686=('nasm')
makedepends_x86_64=('yasm')
install=$pkgname.install
mksource=(https://downloads.sourceforge.net/project/$pkgname/$pkgname/$pkgver/${pkgname}_${pkgver}_src_all.tar.bz2)
source=(https://repo.parabola.nu/other/${_pkgname}/${_pkgname}_${pkgver}_src_all.tar.bz2
        CVE-2016-9296.patch
        CVE-2017-17969.patch
        libre.patch)
mksha256sums=('5eb20ac0e2944f6cb9c2d51dd6c4518941c185347d4089ea89087ffdd6e2341f')
sha256sums=('486e5784de9284c7123b401b3f27e684af772d03fc1fb8cdbbefb8123dc9f841'
            'f9bcbf21d4aa8938861a6cba992df13dec19538286e9ed747ccec6d9a4e8f983'
            'c6af5ba588b8932a5e99f3741fcf1011b7c94b533de903176c7d1d4c02a9ebef'
            'feeeb3b1b7a827b7a675d6c8a363ea6c2499a4f04b3160c7e8d7bba42e0b3c15')

mksource() {
  cd "${srcdir}/${pkgname}_${pkgver}"

  # Remove nonfree unRAR utility files from the source
  rm -rv CPP/7zip/{Archive,Compress}/Rar
  rm -v CPP/7zip/Compress/{Rar{1,2,3}Decoder.{cpp,h},Rar3Vm.{cpp,h},RarCodecsRegister.cpp}
  rm -v CPP/7zip/Crypto/{Rar20Crypto,Rar{,5}Aes}.{cpp,h}
  rm -v Utils/file_Codecs_Rar_so.py
  rm -v DOC/unRarLicense.txt

  # Remove nonfree parent folder icon
  rm CPP/7zip/UI/FileManager/res/ParentFolder.h
}

prepare() {
  cd "$srcdir/${pkgname}_$pkgver"

  # https://sourceforge.net/p/p7zip/bugs/185/
  patch -Np1 -i ../CVE-2016-9296.patch

  # https://sourceforge.net/p/p7zip/bugs/204/
  patch -Np1 -i ../CVE-2017-17969.patch

  if [[ $CARCH = x86_64 ]]; then
    cp makefile.linux_amd64_asm makefile.machine
  elif [[ $CARCH = i686 ]]; then
    cp makefile.linux_x86_asm_gcc_4.X makefile.machine
  else
    cp makefile.linux_any_cpu_gcc_4.X makefile.machine
  fi

  # Remove RAR and parent folder icon references
  patch -Np1 -i ../libre.patch
}

build() {
  cd "$srcdir/${pkgname}_$pkgver"
  make all3 OPTFLAGS="$CFLAGS"
}

package() {
  cd "$srcdir/${pkgname}_$pkgver"

  make install \
    DEST_DIR="$pkgdir" \
    DEST_HOME=/usr \
    DEST_MAN=/usr/share/man

  # Remove documentation for the GUI file manager
  rm -r "$pkgdir/usr/share/doc/p7zip/DOC/MANUAL/fm"

  install -d "${pkgdir}"/usr/share/licenses/p7zip
  ln -s -t "$pkgdir/usr/share/licenses/p7zip/" \
    /usr/share/doc/p7zip/DOC/License.txt

  chmod -R a+r,u+w,a+X "$pkgdir/usr"
}

# vim:set ts=2 sw=2 et:
