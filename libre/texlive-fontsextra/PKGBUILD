# Maintainer (Arch): Rémy Oudompheng <remy.archlinux.org>
# Contributor (Hyperbola): André Silva <emulatorman@hyperbola.info>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Contributor: Isaac David <isacdaavid@at@isacdaavid@dot@info>
# Contributor: Michał Masłowski <mtjm@mtjm.eu>

_pkgname=texlive-fontsextra-libre
pkgname=texlive-fontsextra
pkgver=2018.48561
_revnr=${pkgver#2018.}
pkgrel=1.parabola1
pkgdesc="TeX Live - all sorts of extra fonts, without nonfree add-on packages"
license=('GPL')
arch=(any)
depends=('texlive-core')
groups=('texlive-most')
replaces=('texlive-fontsextra-libre')
conflicts=('texlive-fontsextra-libre')
url='http://tug.org/texlive/'
mksource=("https://sources.archlinux.org/other/texlive/$pkgname-$pkgver-src.zip")
mksha256sums=('7b1db7414f7d2970a5df0fa015c7e06e55ef5e2e77474548b6617a503bd448cc')
noextract=("$pkgname-$pkgver-src.zip")
source=("https://repo.parabola.nu/other/$_pkgname/$_pkgname-$pkgver-src.tar.xz"{,.sig}
        "$pkgname.maps")
options=('!emptydirs')
sha256sums=('60d8351ccd55c32dae703eb07142f4f5376efa95b034773259e56f362635c77a' 'SKIP'
            '20639ccf527a02933a8f354d422047d5155d161312fce46f1abbf27f15967cf7')
validpgpkeys=('1B8C5E87702444D3D825CC8086ED62396D5DBA58' # Omar Vega Ramos <ovruni@gnu.org.pe>
              '38D33EF29A7691134357648733466E12EC7BA943') # Isaac David <isacdaavid@at@isacdaavid@dot@info>

mksource() {
   mkdir $pkgname-$pkgver
   pushd $pkgname-$pkgver
   bsdtar xfv ../$pkgname-$pkgver-src.zip

   # remove nonfree packages
   # no specific free license
   rm -v ogham.tar.xz

   popd
}

prepare() {
   cd $srcdir/$pkgname-$pkgver
   echo -n "   --> extracting all packages... "
   for p in *.tar.xz; do
	   bsdtar -xf $p
   done
   rm -rf {tlpkg,doc,source} || true

   # remove nonfree packages references from package list
   sed -ri '/^ogham /d' CONTENTS
}

package() {
   cd $srcdir/$pkgname-$pkgver
   install -m755 -d $pkgdir/var/lib/texmf/arch/installedpkgs
   sed -i '/^#/d' CONTENTS
   install -m644 CONTENTS $pkgdir/var/lib/texmf/arch/installedpkgs/${pkgname}_${_revnr}.pkgs
   install -m644 $srcdir/$pkgname.maps $pkgdir/var/lib/texmf/arch/installedpkgs/
   install -m755 -d $pkgdir/usr/share
   wanteddirs=$(for d in *; do test -d $d && [[ $d != texmf* ]] && echo $d; done) || true
   for dir in $wanteddirs; do
     find $dir -type d -exec install -d -m755 $pkgdir/usr/share/texmf-dist/'{}' \;
     find $dir -type f -exec install -m644 '{}' $pkgdir/usr/share/texmf-dist/'{}' \;
   done
   if [[ -d texmf-dist ]]; then
     find texmf-dist -type d -exec install -d -m755 $pkgdir/usr/share/'{}' \;
     find texmf-dist -type f -exec install -m644 '{}' $pkgdir/usr/share/'{}' \;
   fi
   if [[ -d $pkgdir/usr/share/texmf-dist/scripts ]]; then
     find $pkgdir/usr/share/texmf-dist/scripts -type f -exec chmod a+x '{}' \;
   fi
}
