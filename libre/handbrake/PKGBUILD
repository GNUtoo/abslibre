# $Id$
# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Sebastien Piccand <sebcactus gmail com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=('handbrake' 'handbrake-cli')
pkgver=1.1.0
pkgrel=1.parabola1
arch=('i686' 'x86_64' 'armv7h')
url="https://handbrake.fr/"
license=('GPL')
makedepends=('intltool' 'python2' 'yasm' 'wget' 'bzip2' 'gcc-libs' 'libnotify'
             'gst-plugins-base' 'gtk3' 'dbus-glib' 'fribidi' 'libass' 'lame'
             'fontconfig' 'freetype2' 'libxml2' 'libogg' 'libvorbis' 'cmake'
             'libtheora' 'libsamplerate' 'ffmpeg' 'x264' 'libx264' 'x265'
             'libvpx' 'libdvdnav' 'jansson' 'librsvg' 'libgudev'
             'libdvdread' 'libbluray')
source=(https://download.handbrake.fr/releases/$pkgver/HandBrake-$pkgver-source.tar.bz2
        https://github.com/HandBrake/HandBrake/releases/download/$pkgver/HandBrake-$pkgver-source.tar.bz2.sig
        handbrake-fix-missing-x265-link-flag.patch
        handbrake-no-download.patch
        handbrake-supress-clip_id.patch)
sha256sums=('a02e7c6f8bd8dc28eea4623663deb5971dcbca1ad59da9eb74aceb481d8c40da'
            'SKIP'
            '05e212e76b6e0b94a04de02cbeb7bf9db607059e19297b5bd3d0d143135285c4'
            'd8116caa83f870803581f0d02e1302b3b8990479d446e4c73bedd35bed531042'
            '6407a96c59baa030a4e5fa46c53ddb3e841c7d4787613d391e032a62600d527f')
validpgpkeys+=('1629C061B3DDE7EB4AE34B81021DB8B44E4A8645') # HandBrake Team <developers@handbrake.fr>

prepare() {
  cd "$srcdir/HandBrake-$pkgver"

  # https://bugs.gentoo.org/show_bug.cgi?id=552792
  patch -Np1 -i ../handbrake-fix-missing-x265-link-flag.patch
  patch -Np1 -i ../handbrake-no-download.patch
  patch -Np1 -i ../handbrake-supress-clip_id.patch

  # Use more system libs
  # Bundled libbluray is kept because it is patched locally
  for _lib in ffmpeg x265 libvpx libdvdnav libdvdread; do
    sed -i "/MODULES += contrib\/$_lib/d" make/include/main.defs
  done
}

build() {
  cd "$srcdir/HandBrake-$pkgver"

  ./configure \
    --prefix=/usr \
    --disable-gtk-update-checks
  make -C build
}

package_handbrake() {
  pkgdesc="Multithreaded video transcoder"
  depends=('bzip2' 'gcc-libs' 'gst-plugins-base' 'libnotify' 'dbus-glib'
           'fribidi' 'libass' 'lame' 'gtk3' 'fontconfig' 'freetype2' 'libxml2'
           'libogg' 'libvorbis' 'libtheora' 'libsamplerate' 'ffmpeg' 'libx264'
           'x265' 'libvpx' 'libdvdnav' 'jansson' 'librsvg' 'libgudev'
           'desktop-file-utils' 'hicolor-icon-theme')
  optdepends=('gst-plugins-good: for video previews'
              'gst-libav: for video previews')

  cd "$srcdir/HandBrake-$pkgver/build"

  make DESTDIR="$pkgdir" install
  rm "$pkgdir/usr/bin/HandBrakeCLI"
}

package_handbrake-cli() {
  pkgdesc="Multithreaded video transcoder (CLI)"
  depends=('bzip2' 'gcc-libs' 'zlib' 'fribidi' 'libass' 'lame' 'libxml2'
           'libtheora' 'libsamplerate' 'ffmpeg' 'libx264' 'x265' 'libvpx'
           'libdvdnav' 'jansson')

  cd "$srcdir/HandBrake-$pkgver/build"
  install -D HandBrakeCLI "$pkgdir/usr/bin/HandBrakeCLI"
}

# vim:set ts=2 sw=2 et:
