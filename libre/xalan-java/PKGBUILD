# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_libname=xalan
pkgname=${_libname}-java
pkgver=2.7.2
pkgrel=1.parabola1
pkgdesc="XSLT processor for transforming XML documents into HTML, text, or other XML document types"
arch=('any')
license=('APACHE')
url="http://xalan.apache.org/xalan-j/index.html"
depends=('java-runtime' 'xerces2-java')
makedepends=('apache-ant' 'jakarta-regexp' 'java-bcel' 'java-cup' 'jlex' 'jh' 'xerces2-java')
source=("http://archive.apache.org/dist/${_libname}/${_libname}-j/source/${_libname}-j_${pkgver//./_}-src.tar.gz"
        "http://repo.maven.apache.org/maven2/${_libname}/${_libname}/${pkgver}/${_libname}-${pkgver}.pom"
        "http://repo.maven.apache.org/maven2/${_libname}/serializer/${pkgver}/serializer-${pkgver}.pom")
md5sums=('74e6ab12dda778a4b26da67438880736'
         '32d9a54317a2495ec8c7f4d9d89dd656'
         '89a3a32915a378a87a113b917dd1144b')

prepare() {
  cd "${srcdir}/${_libname}-j_${pkgver//./_}"
  rm -rv lib/*.jar
  rm -rv tools/*.jar
  sed -i 's/-static //g' build.xml

  ln -sf /usr/share/java/java_cup.jar tools/java_cup.jar
  ln -sf /usr/share/java/JLex.jar tools/JLex.jar
  ln -sf /usr/share/java/bcel.jar lib/BCEL.jar
  ln -sf /usr/share/java/regexp.jar lib/regexp.jar
  ln -sf /usr/share/java/java_cup.jar lib/runtime.jar
#  ln -sf /usr/share/java/xercesImpl.jar lib/xercesImpl.jar
}

build() {
  cd "${srcdir}/${_libname}-j_${pkgver//./_}"

  ant clean xsltc.clean jar xsltc.unbundledjar javadocs docs \
    -Dendorsed.dir=/usr/share/java/
}

package() {
  cd "${srcdir}/${_libname}-j_${pkgver//./_}"

  # Install license file
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/docs" "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "samples" "${pkgdir}/usr/share/doc/${pkgname}/examples"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install ${_libname} ${_libname} ${pkgver} \
    "${srcdir}/${_libname}-${pkgver}.pom" \
    "build/${_libname}.jar" \
    "${_libname}.jar"
  jh mvn-install ${_libname} serializer ${pkgver} \
    "${srcdir}/serializer-${pkgver}.pom" \
    "build/serializer.jar" \
    "serializer.jar"

  ln -s "/usr/share/java/${_libname}.jar" \
    "${pkgdir}/usr/share/java/${_libname}-${pkgver}.jar"
  ln -s "/usr/share/java/serializer.jar" \
    "${pkgdir}/usr/share/java/serializer-${pkgver}.jar"

  install -m644 "build/xsltc.jar" "${pkgdir}/usr/share/java/"
}
