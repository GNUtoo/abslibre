# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=java-batik
pkgver=1.9
pkgrel=1.parabola1
pkgdesc='SVG library for Java.'
arch=('any')
url='http://xmlgraphics.apache.org/batik/'
license=(APACHE)
depends=('java-runtime' 'java-commons-io' 'java-commons-logging' 'java-xmlgraphics-commons'
         'java-xml-commons-external' 'rhino' 'xalan-java' 'xerces2-java')
makedepends=('java-environment' 'apache-ant' 'unzip' 'jh'
             'rhino' 'xalan-java')
source=("https://www.apache.org/dist/xmlgraphics/batik/source/batik-src-$pkgver.tar.gz")
sha256sums=('bef436c0b4ac9a499274a8df9a2769f42ddc6553451dac9a9514b4f55d06c6a3')

prepare() {
  cd "batik-$pkgver"
  rm -rv lib/*
  rm -rv documentation-sources/content/demo/*.jar
  ln -sf /usr/share/java/xml-apis.jar lib/xml-apis.jar
  ln -sf /usr/share/java/xml-apis-ext.jar lib/xml-apis-ext.jar
  #ln -sf /usr/share/java/xalan.jar lib/xalan.jar
  #ln -sf /usr/share/java/xercesImpl.jar lib/xercesImpl.jar
  ln -sf /usr/share/java/xmlgraphics-commons.jar lib/xmlgraphics-commons.jar
}

build() {
  cd "batik-$pkgver"
#  ant all-jar libs-jar ext-jar transcoder-jar
  ant jars libs-jar all-jar
}

package() {
  cd "batik-$pkgver/batik-$pkgver"

  # Install license file
  install -Dm644 ../LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  for artifact in $(find . -name "batik-*-*.jar" ! -name '*-libs-*.jar' | sed 's/-[0-9.]*\.jar$//'); do
    _dir=$(dirname $artifact)
    _artifact=$(basename $artifact)

    jh mvn-install "org.apache.xmlgraphics" ${_artifact} $pkgver \
      "../${_artifact}/pom.xml" \
      "${_dir}/${_artifact}-$pkgver.jar" \
      "${_artifact}-$pkgver.jar"

    # Symlink them to /usr/share/java
    ln -s "/usr/share/java/${_artifact}-$pkgver.jar" \
      "$pkgdir/usr/share/java/${_artifact}.jar"
  done

  # Install batik-all.jar and batik-libs.jar
  install -d "$pkgdir/usr/share/java/batik"
  install -m644 "lib/batik-all-$pkgver.jar" \
    "$pkgdir/usr/share/java/batik/"
  install -m644 "lib/batik-libs-$pkgver.jar" \
    "$pkgdir/usr/share/java/batik/"

  ln -s "/usr/share/java/batik/batik-all-$pkgver.jar" \
    "$pkgdir/usr/share/java/batik/batik-all.jar"
  ln -s "/usr/share/java/batik/batik-libs-$pkgver.jar" \
    "$pkgdir/usr/share/java/batik/batik-libs.jar"
}
