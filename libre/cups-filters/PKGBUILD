# $Id$
# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=cups-filters
pkgver=1.21.3
pkgrel=2
pkgrel+=.par1
pkgdesc="OpenPrinting CUPS Filters"
pkgdesc+=", without foomatic-db-nonfree recommendation"
arch=('x86_64')
arch+=('i686' 'armv7h')
url="https://wiki.linuxfoundation.org/openprinting/cups-filters"
license=('custom')
depends=('lcms2' 'poppler' 'qpdf' 'imagemagick' 'liblouis' 'ijs' 'libcups>=2.2.6-2' 'udev')
depends+=('poppler=0.70.1')
makedepends=('ghostscript' 'ttf-dejavu' 'python' 'mupdf-tools') # ttf-dejavu for make check
optdepends=('ghostscript: for non-PostScript printers to print with CUPS to convert PostScript to raster images'
	    'foomatic-db: drivers use Ghostscript to convert PostScript to a printable form directly'
	    'foomatic-db-engine: drivers use Ghostscript to convert PostScript to a printable form directly'
	    'antiword: needed to convert MS Word documents (requires also docx2txt)')
backup=(etc/cups/cups-browsed.conf)
source=(https://www.openprinting.org/download/cups-filters/$pkgname-$pkgver.tar.xz
        cups-filters-poppler-0.69.0.patch::https://github.com/OpenPrinting/cups-filters/commit/6b0747c163.patch)
sha256sums=('f5a61222148f68d0afc3bb5960eda167f9bcd0055dcd4c5c4a6909f1a79126e0'
            'c68a773ec7d44f2ad532fbe1d6cb3e4825300dcb626461d4bebdce911da24f1a')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i ../cups-filters-poppler-0.69.0.patch
}

build() {
  cd $pkgname-$pkgver

  # Ignore const-related errors (remove once fixed upstream)
  CXXFLAGS+=' -fpermissive'

  ./configure --prefix=/usr  \
    --sysconfdir=/etc \
    --sbindir=/usr/bin \
    --localstatedir=/var \
    --with-rcdir=no \
    --enable-avahi \
    --with-browseremoteprotocols=DNSSD,CUPS \
    --enable-auto-setup-driverless \
    --with-test-font-path=/usr/share/fonts/TTF/DejaVuSans.ttf
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir/" install
  
  # add upstream systemd support file
  install -Dm644 utils/cups-browsed.service ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  sed -i "s|/usr/sbin/cups-browsed|/usr/bin/cups-browsed|" ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  sed -i "s|cups.service|org.cups.cupsd.service|g" ${pkgdir}/usr/lib/systemd/system/cups-browsed.service
  
  # use cups group from cups pkg FS#56818
  chgrp -R 209 ${pkgdir}/etc/cups

  # license
  mkdir -p "${pkgdir}"/usr/share/licenses/${pkgname}
  install -m644 "${srcdir}"/${pkgname}-${pkgver}/COPYING "${pkgdir}"/usr/share/licenses/${pkgname}/
}
