# Maintainer: Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor (Arch): Robson Peixoto
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgname=unzip-libre
pkgname=unzip
pkgver=6.0
_pkgver=${pkgver/./}
pkgrel=12.parabola3
pkgdesc='For extracting and viewing files in .zip archives'
url='http://www.info-zip.org/UnZip.html'
arch=('i686' 'x86_64' 'armv7h')
license=('custom')
depends=('bzip2' 'bash')
conflicts=("${_pkgname}")
replaces=("${_pkgname}")
mksource=("http://downloads.sourceforge.net/infozip/${pkgname}${_pkgver}.tar.gz"
          'match.patch')
source=("https://repo.parabola.nu/other/${_pkgname}/${pkgname}${_pkgver}-libre.tar.gz"
        'overflow-fsize.patch'
        'cve20149636.patch'
        'test_compr_eb.patch'
        'getZip64Data.patch'
        'crc32.patch'
        'empty-input.patch'
        'csiz-underflow.patch'
        'nextbyte-overflow.patch')
mksha512sums=('bbd89e60a0095bf82ae7df8de97cd235e4673e5d9397aa11d584a2cdb0c7b82dc692f68dd24bd3fe0433c2a21ae1a7249bd16aae6011b41530637029275bac17'
            '69df98c4b5279ae5d632a6878f471f8deb420a16e9fc879bb25f15c4400cce15617e40a662d4be1a3888e28bb7cef0f3b70e160cf02fd9dc9c703416e36261d8')
sha512sums=('bbd89e60a0095bf82ae7df8de97cd235e4673e5d9397aa11d584a2cdb0c7b82dc692f68dd24bd3fe0433c2a21ae1a7249bd16aae6011b41530637029275bac17'
            'c6e06fdd95c74a53bc5f774ae84427701d09374173342678ee75ff6dce5c1fab8f61f6a2f0fe2ede4859fc854869386869293f99573a11308536a45ce5f969c3'
            '7e5274db1d0e9b1db87ce543ddb4edea67cea193ee5394a5a46f3813169c33508cbea96cc0ce88eb4ffc64b21df02c18724d0fe8f7d2814954233f646c386b3a'
            '4283d5da3a0403a178295602a5dab7f6d8e2a885c528b85254903e90474cb72a0ed766aad0fce735353adb57c37ba8992e394d5ef6a24a52ed11677d03952ce9'
            'c70609994ac0f056b9cb2797884036f3aa29a542c36b7c259ce99e046a0362b2d81ab4fd08c10d5de25d6799e7aa40a4813dd3070e11fe840f142d6b14db606d'
            'e74c792aa7d45a37a43d90d732257fe3a5efae6cfd9a0d4d853bb7c5decd10c8dd7b5cdb919b51404e136844899304efc07e93e09ae4cbbce52352b39e1ed82c'
            'a9be2b9a7bdb42b3f4c1d92cba9c55e2731fdf5bac46b50caadec060a12e474ac0e3fcb0ff8dfaac086a5c8651c6f5602d191af13a1947b7ffae6269141ea5fe'
            '21ec0286aaa3154c61025a33d66c10f901ee57216d20524f594052a13271ee32236686375b9050f2b0228f3718135b9159190ff6869a294363cd65b066117c40'
            '77f228b4135848b5259ef1eb926a242d8deb8ce54ff02533d27541f5f1e737e72a52f50b5a291d7a266eaee677e1aec52e97fda962926462697551286d14c49d')

mksource() {
	cd "${srcdir}/${pkgname}${_pkgver}"

	# from http://bzr.trisquel.info/package-helpers/trunk/annotate/head%3A/helpers/natty/DATA/unzip/match.patch
	patch -Np0 -i ${srcdir}/match.patch
}

prepare() {
	cd "${srcdir}/${pkgname}${_pkgver}"
	sed -i "/MANDIR =/s#)/#)/share/#" unix/Makefile
	patch -p1 -i ../overflow-fsize.patch # FS#44171
	patch -p1 -i ../cve20149636.patch # FS#44171
	patch -i ../test_compr_eb.patch # FS#43391
	patch -i ../getZip64Data.patch # FS#43300
	patch -i ../crc32.patch # FS#43300
	patch -p1 -i ../empty-input.patch # FS#46955
	patch -p1 -i ../csiz-underflow.patch # FS#46955
	patch -p1 -i ../nextbyte-overflow.patch # FS#46955
}

build() {
	cd "${srcdir}/${pkgname}${_pkgver}"

	# DEFINES, make, and install args from Debian
	DEFINES='-DACORN_FTYPE_NFS -DWILD_STOP_AT_DIR -DLARGE_FILE_SUPPORT \
		-DUNICODE_SUPPORT -DUNICODE_WCHAR -DUTF8_MAYBE_NATIVE -DNO_LCHMOD \
		-DDATE_FORMAT=DF_YMD -DUSE_BZIP2 -DNOMEMCPY -DNO_WORKING_ISPRINT'

	make -f unix/Makefile prefix=/usr \
		D_USE_BZ2=-DUSE_BZIP2 L_BZ2=-lbz2 \
		LF2="$LDFLAGS" CF="$CFLAGS $CPPFLAGS -I. $DEFINES" \
		unzips
}

package() {
	cd "${srcdir}/${pkgname}${_pkgver}"
	make -f unix/Makefile prefix="${pkgdir}"/usr install
	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
