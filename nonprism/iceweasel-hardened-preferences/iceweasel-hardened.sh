#!/bin/sh

##############################################
# Iceweasel-Hardened Jail and Cleaner Script #
##############################################
echo "Closing any other instances of Iceweasel to avoid crashes..."
pkill -x iceweasel
wait
echo "Copying Hardened Prefs..."
cp /usr/lib/iceweasel/browser/defaults/preferences/iceweasel-hardened.prefs /usr/lib/iceweasel/browser/defaults/preferences/firefox-branding.js
wait
echo "Waking the Iceweasel..."

# Trap cleaner function for Iceweasel exit cleaning
function finish {
echo "Removing hardened preferences..."
echo "" > /usr/lib/iceweasel/browser/defaults/preferences/firefox-branding.js
    }

## Firejail Iceweasel startup
TZ="UTC" /usr/bin/firejail --profile=/etc/firejail/firefox.profile --noroot --nogroups --caps.drop=all --private-etc=nsswitch.conf,resolv.conf --private-bin=sh,bash,iceweasel --private-tmp --private-dev /usr/bin/iceweasel --private-window -no-remote

## Exiting Iceweasel triggers the trap
trap finish EXIT
