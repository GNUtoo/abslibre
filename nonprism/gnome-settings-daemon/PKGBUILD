# $Id$
# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Andreas Grapentin <andreas@grapentin.org>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Isaac David <isacdaavid@at@isacdaavid.info>

# parabola changes and rationale:
#  - removed geoclue2 support

pkgname=gnome-settings-daemon
pkgver=3.26.2
pkgrel=1.nonprism1
pkgdesc="GNOME Settings Daemon, without geoclue2 support"
url="https://git.gnome.org/browse/gnome-settings-daemon"
arch=(x86_64 i686 armv7h)
license=(GPL)
depends=(dconf gnome-desktop gsettings-desktop-schemas libcanberra-pulse libnotify libsystemd
         libwacom pulseaudio pulseaudio-alsa upower librsvg libgweather geocode-glib nss
         libgudev gtk3-print-backends libnm)
makedepends=(intltool xf86-input-wacom libxslt docbook-xsl python git gnome-common)
groups=(gnome)
_commit=b4a1b1ab4da2be6628811a763376fc39c116cdbe  # tags/GNOME_SETTINGS_DAEMON_3_26_2^0
source=("git+https://git.gnome.org/browse/gnome-settings-daemon#commit=$_commit"
        "git+https://git.gnome.org/browse/libgnome-volume-control"
        nonprism.patch)
sha256sums=('SKIP'
            'SKIP'
            '6ac18c459d9d2541be9bf9cf94f31c844920dd627f82c159d43490bb6f905ad4')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/^GNOME_SETTINGS_DAEMON_//;s/_/./g;s/-/+/g'
}

prepare() {
  cd $pkgname

  git submodule init
  git config --local submodule."panels/media-keys/gvc".url "$srcdir/libgnome-volume-control"
  git submodule update

  patch -p1 -i $srcdir/nonprism.patch
  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd $pkgname

  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --libexecdir=/usr/lib/$pkgname --disable-static

  #https://bugzilla.gnome.org/show_bug.cgi?id=656231
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}
