# Maintainer: Luke Shumaker <lukeshu@parabola.nu>

pkgbase=libsrs2
pkgname=(libsrs2 libsrs2-python libsrs2-perl)
pkgver=1.0.18
pkgrel=2
pkgdesc="Implementation of the Sender Rewriting Scheme (SRS) for SMTP forwarding"
arch=('i686' 'x86_64' 'armv7h')
url="http://www.libsrs2.org/"
license=('GPL2' 'BSD3')
makedepends=(python perl-mail-srs)
source=("http://www.libsrs2.org/srs/$pkgbase-$pkgver.tar.gz")
sha1sums=('db9452e5207bb573eca4b3409c201f1e0275d300')


build() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  CFLAGS="$CFLAGS -fPIC" ./configure --prefix=/usr
  make -j1

  cd "${srcdir}/${pkgbase}-${pkgver}/perl"
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

check() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  make check

  cd "${srcdir}/${pkgbase}-${pkgver}/perl"
  make test
}

package_libsrs2() {
  cd "${srcdir}/${pkgbase}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  install -Dm644 -t "$pkgdir/usr/share/licenses/$pkgname" COPYING LICENSE.*
}

package_libsrs2-python() {
  provides=(python-libsrs2)
  depends=(libsrs2 python)

  cd "${srcdir}/${pkgbase}-${pkgver}/python"
  python setup.py install --root="$pkgdir/" --optimize=1

  cd ..
  install -Dm644 -t "$pkgdir/usr/share/licenses/$pkgname" COPYING LICENSE.*
}

package_libsrs2-perl() {
  license=('GPL' 'PerlArtistic')
  provides=(perl-mail-srs-xs)
  depends=(libsrs2 perl-mail-srs)

  cd "${srcdir}/${pkgbase}-${pkgver}/perl"
  make DESTDIR="${pkgdir}" install
}

# vim:set ts=2 sw=2 et:
