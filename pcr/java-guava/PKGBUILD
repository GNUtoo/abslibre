# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=guava
pkgname=java-${_pkgname}
pkgver=19.0
pkgrel=1
pkgdesc='Suite of Google common libraries for Java'
arch=('any')
url="https://github.com/google/guava"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'java-animal-sniffer' 'jsr305' 'jh')
source=("https://github.com/google/${_pkgname}/archive/v${pkgver}.tar.gz"
        'guava-ignore_j2objc_annotations.patch')
sha256sums=('67a8f7a22c26bdbfc0455a3b74bdee753e170b30f3efb8437cb5aee8c62463ac'
            'e03f4dbfb92dcd688e5f8fd01c8fb0c2a5cb601a88af4d2aaa09afdd388e9cab')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  rm -rv {guava-gwt,guava-testlib,guava-tests,util}
  mkdir -p "${_pkgname}/build/classes"

  patch -Np1 -i "${srcdir}/guava-ignore_j2objc_annotations.patch"
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}/${_pkgname}"

  CLASSPATH="/usr/share/java/animal-sniffer-annotations.jar:/usr/share/java/jsr305.jar"
  javac -cp $CLASSPATH -d "build/classes" -encoding UTF-8 \
    $(find "src" -name \*.java)
  javadoc -classpath $CLASSPATH -d "build/javadoc" -encoding UTF-8 \
    -sourcepath src -subpackages com

  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}/${_pkgname}"

  # Install license file
  install -Dm644 "../COPYING" "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "com.google.guava" "${_pkgname}-parent" ${pkgver} \
    "${srcdir}/${_pkgname}-${pkgver}/pom.xml"
  jh mvn-install "com.google.guava" ${_pkgname} ${pkgver} \
    "pom.xml" \
    "${_pkgname}.jar" \
    "${_pkgname}-${pkgver}.jar"

  ln -s "/usr/share/java/${_pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}.jar"
}
