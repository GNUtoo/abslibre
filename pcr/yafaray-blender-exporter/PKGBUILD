# Maintainer (AUR): Jeremy Johnathan Rodent <narf@x201t>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# parabola changes and rationale:
#  - adapted from yafaray-experimental-blender-exporter on AUR
#  - renamed tarball to avoid collisions
#  - added replaces for previous pkgname 'blender-addon-yafaray' (2018-02-01)

pkgname='yafaray-blender-exporter'
pkgver='3.3.0'
pkgrel='1'
pkgdesc="Yafaray exporter plugin for blender"
arch=('any')
url='http://yafaray.org'
license=('GPL')
depends=('blender')
optdepends=('yafaray: for the actual rendering part.')
replaces=('blender-addon-yafaray')
source=("$pkgname-$pkgver.tar.gz::https://github.com/YafaRay/Blender-Exporter/archive/v$pkgver.tar.gz")
md5sums=('b1eb1bb0eb56541c6c1990f02b15603f')

_srcdir="Blender-Exporter-$pkgver"
_blender_ver="2.78"

prepare() {
  cd "$_srcdir"
  sed -i 's/ctypes.cdll.LoadLibrary(os.path.join(BIN_PATH, dll))/pass/' '__init__.py'
  sed -i 's#PLUGIN_PATH =.*#PLUGIN_PATH = "/usr/lib/yafaray"#' '__init__.py'
}

package() {
  cd "$_srcdir"
  dstdir="$pkgdir/usr/share/blender/$_blender_ver/scripts/addons/yafaray"
  mkdir -p "$dstdir"
  cp -r * "$dstdir"
}

# vim:set ts=2 sw=2 et:
