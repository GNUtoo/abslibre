# Maintainer (Artix): artoo <artoo@manjaro.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Link Dupont <link@subpop.net>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>
# Maintainer: David P. <megver83@parabola.nu>

pkgname=dbus-elogind
_pkgname=${pkgname/-elogind}
pkgver=1.12.6
pkgrel=1
pkgdesc="Freedesktop.org message bus system"
url="https://wiki.freedesktop.org/www/Software/dbus/"
arch=(i686 x86_64 armv7h)
license=(GPL custom)
provides=('libdbus' "dbus=$pkgver" "dbus-x11=$pkgver")
conflicts=('libdbus' 'dbus' 'dbus-x11')
depends=('libx11' 'expat' 'libelogind')
makedepends=(elogind xmlto docbook-xsl python yelp-tools doxygen git autoconf-archive graphviz)
optdepends=('dbus-openrc: dbus initscript')
_commit=a723baa2bc7e91fc9b1926cda1ce53e9301de4d8  # tags/dbus-1.12.6^0
source=("git+https://anongit.freedesktop.org/git/dbus/dbus#commit=$_commit"
        'dbus.sysusers'
        'dbus-enable-elogind.patch')
sha256sums=('SKIP'
            '9bc34a20595df8869d43a8d9af74cbded999c9a004ec12ff8ce07d58d81018d8'
            'faffcaa5b295f49fcedeed2c9ece5298949096be3062fd99a4bf1a6ac3ad1ea0')
validpgpkeys=('DA98F25C0871C49A59EAFF2C4DE8FF2A63C7CC90'  # Simon McVittie <simon.mcvittie@collabora.co.uk>
              '3C8672A0F49637FE064AC30F52A43A1E4B77B059') # Simon McVittie <simon.mcvittie@collabora.co.uk>

pkgver() {
  cd $_pkgname
  git describe --tags | sed 's/^dbus-//;s/-/+/g'
}

prepare() {
  cd $_pkgname

  # Reduce docs size
  printf '%s\n' >>Doxyfile.in \
    HAVE_DOT=yes DOT_IMAGE_FORMAT=svg INTERACTIVE_SVG=yes

  patch -Np1 -i ${srcdir}/dbus-enable-elogind.patch

  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd $_pkgname
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --libexecdir=/usr/lib/dbus-1.0 --with-dbus-user=dbus \
      --with-system-pid-file=/run/dbus/pid \
      --with-system-socket=/run/dbus/system_bus_socket \
      --with-console-auth-dir=/run/console/ \
      --enable-inotify --disable-static \
      --disable-verbose-mode --disable-asserts \
      --without-systemdsystemunitdir \
      --disable-systemd --disable-user-session \
      --enable-elogind --enable-x11-autolaunch
  make

}

check() {
  cd $_pkgname
  make check # will fail with libremakepkg if building without -N
}

package() {
  cd $_pkgname

  make DESTDIR="$pkgdir" install

  rm -r "$pkgdir/var/run"

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$_pkgname/COPYING"

  # We have a pre-assigned uid (81)
  install -Dm644 "$srcdir/dbus.sysusers" "$pkgdir/usr/lib/sysusers.d/dbus.conf"

  # Remove docs
  rm -r "$pkgdir/usr/share/doc"
}
