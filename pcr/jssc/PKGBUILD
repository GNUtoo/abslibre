# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=jssc
pkgver=2.8.0
pkgrel=1
pkgdesc="Java Simple Serial Connector; for working with serial ports from Java"
arch=('i686' 'x86_64')
url="https://github.com/scream3r/java-simple-serial-connector"
license=('LGPL')
depends=('java-runtime')
makedepends=('java-environment')
source=("https://github.com/scream3r/java-simple-serial-connector/archive/v$pkgver.tar.gz")
sha256sums=('c3287bfc31ea81929739271b087b833fb7050686af6a90ec953c5b2f41b4d42d')

prepare() {
  cd "$srcdir/java-simple-serial-connector-$pkgver"
  rm -rv src/{cpp/windows,java/libs}
  mkdir -p build/classes
}

build() {
  cd "$srcdir/java-simple-serial-connector-$pkgver"
  export JAVA_HOME=/usr/lib/jvm/default
  g++ $CXXFLAGS $CPPFLAGS $LDFLAGS -I"$JAVA_HOME/include" -I"$JAVA_HOME/include/linux" \
     -fPIC -shared -o libjSSC-$pkgver.so src/cpp/_nix_based/jssc.cpp

  javac -d build/classes -encoding UTF-8 $(find src/java -name \*.java)
  javadoc -d build/javadoc -encoding UTF-8 -sourcepath src/java -subpackages jssc
  jar -cvf "$pkgname-$pkgver.jar" -C build/classes .
}

package() {
  cd "$srcdir/java-simple-serial-connector-$pkgver"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install jar
  install -Dm644 "$pkgname-$pkgver.jar" \
    "$pkgdir/usr/share/java/$pkgname-$pkgver.jar"
  ln -s "/usr/share/java/$pkgname-$pkgver.jar" \
    "$pkgdir/usr/share/java/$pkgname.jar"

  install -Dm644 "libjSSC-$pkgver.so" \
    "$pkgdir/usr/lib/jni/libjSSC-$pkgver.so"
}
