# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=javamail
pkgver=1.5.6
pkgrel=1
pkgdesc="JavaMail API Reference Implementation"
arch=('any')
url="https://javamail.java.net"
license=('CDDL, GPL2 with Classpath Exception')
depends=('java-runtime')
makedepends=('apache-ant' 'jh' 'tomcat8')
source=("https://java.net/downloads/$pkgname/source/$pkgname-$pkgver-src.zip")
sha256sums=('6754e4c3f3f2a50fc4bc9c6f4e041dd91dc99a716d38bceeac0f5327e2bf73f5')

prepare() {
  cd $srcdir
  cp -v mail/pom.xml javax.mail.pom
  cp -v mailapijar/pom.xml javax.mail-api.pom
  cp -v mailhandler/pom.xml logging-mailhandler.pom
}

build() {
  cd $srcdir
  ant jars docs \
    -Djavaee.jar=/usr/share/java/tomcat8/servlet-api.jar \
    -Drelease.mail.jar=javax.mail.jar \
    -Drelease.version=$pkgver

  jar -cvf javax.mail-api.jar -C mail/target/classes javax
  jar -cvf logging-mailhandler.jar -C mail/target/classes com/sun/mail/util/logging
}

package() {
  cd $srcdir

  # Install license file
  install -Dm644 mail/src/main/resources/META-INF/LICENSE.txt \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r target/release/docs/javadocs "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  for artifact in "all" "dsn" "gimap" "imap" "mailapi" "pop3" "smtp"; do
    if [[ -f "target/release/lib/$artifact.jar" ]]; then
      # This artifact has a jar file
      jh mvn-install com.sun.mail $artifact $pkgver \
        "$artifact/pom.xml" \
        "target/release/lib/$artifact.jar" \
        "$artifact-$pkgver.jar"

      # Symlink them to /usr/share/java
      ln -s "/usr/share/java/$artifact-$pkgver.jar" \
        "$pkgdir/usr/share/java/$artifact.jar"
    else
      # This artifact is just a pom
      jh mvn-install com.sun.mail $artifact $pkgver \
        "pom.xml"
    fi
  done

  for artifact in "javax.mail" "logging-mailhandler"; do
    jh mvn-install com.sun.mail $artifact $pkgver \
      "$artifact.pom" \
      "$artifact.jar" \
      "$artifact-$pkgver.jar"

    # Symlink them to /usr/share/java
    ln -s "/usr/share/java/$artifact-$pkgver.jar" \
      "$pkgdir/usr/share/java/$artifact.jar"
  done

  jh mvn-install javax.mail javax.mail-api $pkgver \
    "javax.mail-api.pom" \
    "javax.mail-api.jar" \
    "javax.mail-api-$pkgver.jar"
  ln -s "/usr/share/java/javax.mail-api-$pkgver.jar" \
    "$pkgdir/usr/share/java/javax.mail-api.jar"
}
