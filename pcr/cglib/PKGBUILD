# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=cglib
pkgver=3.1
pkgrel=1
pkgdesc="Code generation library for Java"
arch=('any')
url="https://github.com/cglib/cglib"
license=('APACHE')
depends=('java-runtime')
makedepends=('apache-ant' 'java-asm' 'java-jarjar' 'jh')
source=("https://github.com/${pkgname}/${pkgname}/archive/RELEASE_${pkgver//./_}.tar.gz"
        "https://repo1.maven.org/maven2/${pkgname}/${pkgname}/${pkgver}/${pkgname}-${pkgver}.pom"
        "https://repo1.maven.org/maven2/${pkgname}/${pkgname}-nodep/${pkgver}/${pkgname}-nodep-${pkgver}.pom"
        "cglib-jarjar.patch")
md5sums=('6886df33b07c65e23b6a8aed9ae01ae4'
         'cdcb12866a7e341e89ad7c3bf9c19b71'
         '41cbb7bc608af67c661ebbd1ac936e23'
         '09f089fc00ad12f8df31d35533444152')

prepare() {
  cd "${srcdir}/${pkgname}-RELEASE_${pkgver//./_}"
  find . -name \*.jar -delete
  patch -Np1 -i "${srcdir}/cglib-jarjar.patch"
}

build() {
  cd "${srcdir}/${pkgname}-RELEASE_${pkgver//./_}"
  ant jar javadoc \
   -lib "/usr/share/java/asm-4.jar" \
   -lib "/usr/share/java/asm-util-4.jar" \
   -lib "/usr/share/java/jarjar.jar"
}

package() {
  cd "${srcdir}/${pkgname}-RELEASE_${pkgver//./_}"

  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "docs" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install ${pkgname} ${pkgname} ${pkgver} \
    "${srcdir}/${pkgname}-${pkgver}.pom" \
    "${srcdir}/${pkgname}-RELEASE_${pkgver//./_}/dist/${pkgname}-${pkgver}.jar" \
    "${pkgname}.jar"
  jh mvn-install ${pkgname} "${pkgname}-nodep" ${pkgver} \
    "${srcdir}/${pkgname}-nodep-${pkgver}.pom" \
    "${srcdir}/${pkgname}-RELEASE_${pkgver//./_}/dist/${pkgname}-nodep-${pkgver}.jar" \
    "${pkgname}-nodep.jar"

  ln -s "/usr/share/java/${pkgname}.jar" \
    "${pkgdir}/usr/share/java/${pkgname}-${pkgver}.jar"
  ln -s "/usr/share/java/${pkgname}-nodep.jar" \
    "${pkgdir}/usr/share/java/${pkgname}-nodep-${pkgver}.jar"
}
