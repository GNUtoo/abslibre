# Maintainer: Luke Shumaker <lukeshu@lukeshu.com>

pkgname=librevpn
pkgver=1.0.0rc
pkgrel=2
pkgdesc='Free Tinc-based Virtual Private Network'
url=http://$pkgname.org.ar/
license=('AGPL3' 'MIT') # AGPL3+, but the vendored generate-ipv6-address is MIT
arch=(x86_64 i686 armv7h)
depends=(avahi bash tinc)
makedepends=(pandoc)
optdepends=(
  #'python2-bottle: for `etc/keyserver`'
  'graphviz: for `lvpn graph` and `lvpn tinc2dot`'
  'libnatpmp: for `lvpn install-script port-forwarding`'
  'miniupnpc: for `lvpn install-script port-forwarding`'
  'networkmanager: for automatic reload on network-up'
  'openssh: for `lvpn push`'
  'python2: for `lvpn avahi-publish-alias`'
  'rsync: for `lvpn install`'
  'ruby: for `lvpn graph`'
  'smtp-forwarder: for `lvpn send-email`'
  'sudo: for `lvpn d3`, for priv-sep in other commands'
)
options=(emptydirs)
source=("$pkgname-$pkgver.tar.gz::https://github.com/LibreVPN/$pkgname/archive/$pkgver.tar.gz")
sha512sums=('253263df36b9a38671e01e8742ecbe783ff33e02e94f25c4a0d2c1a8843a6543e46a3fc63020379101d22ce6e5bd49fc80ba142be5dee51f737d7e62ac45ecdf')

prepare() {
  cd $pkgname-$pkgver

  # remove pre-compiled binary files
  find -name '*.pyc' -delete
  rm -f -- bin/*-generate-ipv6-address bin/natpmpc bin/upnpc
  # remove other generated files
  find doc -name '*.1' -delete
  rm lvpn

  sed -i '/BEADLE/s/HOSTS/BEADLE/' lvpn.in
}

build() (
  cd $pkgname-$pkgver

  unset TEXTDOMAIN TEXTDOMAINDIR
  make PREFIX=/usr
)

package() (
  cd $pkgname-$pkgver

  unset TEXTDOMAIN TEXTDOMAINDIR
  make PREFIX=/usr TARGET="$pkgdir" install

  install -Dm644 etc/generate-ipv6-address-0.1/generate-ipv6-address.man "$pkgdir"/usr/share/man/man8/generate-ipv6-address.8
  install -d "$pkgdir"/etc/logrotate.d
  install -d "$pkgdir"/etc/NetworkManager/dispatcher.d
  ln -sT hosts $pkgdir/usr/share/lvpn/beadle

  local cmd
  for cmd in avahi-publish-alias graph tinc2dot; do
    install -Dm755 bin/$cmd "$pkgdir"/usr/lib/lvpn/lvpn-$cmd
  done

  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n '/Copyright/,/\*\//p' \
    < etc/generate-ipv6-address-0.1/generate-ipv6-address.c \
    > "$pkgdir/usr/share/licenses/$pkgname/LICENSE.generate-ipv6-address.txt"
)

# vim:set ts=2 sw=2 et:
