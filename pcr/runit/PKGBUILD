# Maintainer: David P. <megver83@parabola.nu>
# Contributor: Luke Shumaker <lukeshu@parabola.nu>
# Maintainer (Artix): Chris Cromer <chris@cromer.cl>

pkgbase=runit
pkgname=('runit' 'runit-replaceinit')
pkgver=2.1.2
pkgrel=2
arch=('armv7h' 'i686' 'x86_64')
url='http://smarden.org/runit/'
license=('BSD3')
source=("http://smarden.org/${pkgname}/${pkgbase}-${pkgver}.tar.gz")
sha512sums=('a18773ebf1aa22305dd89ed67363165b9fcf86c192b2be4e268d08005dd82d51265160c637abe072f2f2e378c4b315a75bd3d3e602c3e75bdd451a3b0190f8cf')

build() {
  cd "${srcdir}/admin/${pkgbase}-${pkgver}"
  package/compile
}

check() {
  cd "${srcdir}/admin/${pkgbase}-${pkgver}"
  package/check
}

package_runit() {
  pkgdesc='Cross-platform Unix init scheme with service supervision'
  optdepends=('runit-replaceinit')
  install=runit.install

  provides=('runit-doc')
  conflicts=('runit-doc')
  replaces=('runit-doc')

  cd "${srcdir}/admin/${pkgbase}-${pkgver}"

  # `package/install`... but use FHS and obey $pkgdir
  local command
  while read -r command; do
    install -Dm755 -t "$pkgdir"/usr/bin -- "command/$command"
  done < package/commands

  # `package/install-man`... but use FHS, obey $pkgdir, and let makepkg take care of gzip
  mkdir -p "${pkgdir}"/usr/share/man/man8
  install -Dm644 -t "${pkgdir}"/usr/share/man/man8 -- man/*.8

  # doc/runlevels.html
  mkdir -p "${pkgdir}"/etc/runit/runsvdir/default
  ln -sT etc/runit/runsvdir/current "${pkgdir}"/service
  # Don't include /etc/runit/runsvdir/current in the package; let it
  # be managed at run-time by `runsvchdir`.

  # doc/upgrade.html says to link /service->/etc/service FHS systems,
  # but per doc/runlevels.html, /service is already a symlink.
  ln -sT runit/runsvdir/current "${pkgdir}"/etc/service

  # doc/upgrade.html, since 1.4
  mkdir -p "${pkgdir}"/etc/sv

  # install other assets to their FHS location, rather than their slashpackage location
  install -d "$pkgdir/usr/share/doc"
  cp -rLT doc "$pkgdir/usr/share/doc/$pkgname"
  install -Dm644 -t "$pkgdir/usr/share/licenses/$pkgname" -- package/COPYING
}

package_runit-replaceinit() {
  pkgdesc='Use runit as a sysvinit replacment (base configuration)'
  depends=('runit' 'sh' 'util-linux')
  provides=('init')
  conflicts=('init')
  backup=(
    etc/runit/{1,2,3,ctrlaltdel}
    etc/runit/runsvdir/default/getty-5
    etc/sv/getty-5/{run,finish}
  )

  install -d "${pkgdir}"/usr/bin "${pkgdir}"/etc/sv "${pkgdir}"/etc/runit/runsvdir/default

  cd "${srcdir}/admin/${pkgbase}-${pkgver}"

  # doc/replaceinit.html
  mkdir -p "$pkgdir"/etc/runit
  cp -p etc/debian/[123] "$pkgdir"/etc/runit/
  mkdir -p "$pkgdir"/etc/sv/getty-5
  cp -p etc/debian/getty-tty5/{run,finish} "$pkgdir"/etc/sv/getty-5/
  cp -p etc/debian/ctrlaltdel "$pkgdir"/etc/runit/
  ln -s /etc/sv/getty-5 "$pkgdir"/etc/runit/runsvdir/default/
  ln -sT runit-init "${pkgdir}"/usr/bin/init

  # doc/runlevels.html
  sed -i '/^exec env/{
    irunsvchdir default >/dev/null
    i
  }' "${pkgdir}"/etc/runit/2

  # extra

  # change from "getty RATE PORT TERM" to "agetty PORT RATE TERM"
  sed -ri 's,\bgetty (\S+) (\S+) (\S+),agetty \2 \1 \3,' \
      -- "$pkgdir"/etc/sv/getty-5/run

  install -Dm644 -t "$pkgdir/usr/share/licenses/$pkgname" -- package/COPYING
}
