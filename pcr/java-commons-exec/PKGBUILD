# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=commons-exec
pkgname=java-${_pkgname}
pkgver=1.3
pkgrel=1
pkgdesc="API for dealing with external process execution and environment management"
arch=('any')
url="http://commons.apache.org/exec/"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://archive.apache.org/dist/commons/exec/source/${_pkgname}-$pkgver-src.tar.gz")
sha256sums=('db0742fa59b7d69375e5d99e827aee0bf06ac527aa6def63618950e40c24cb9f')

prepare() {
  cd "$srcdir/${_pkgname}-$pkgver-src"
  mkdir -p build/classes
}

build() {
  cd "$srcdir/${_pkgname}-$pkgver-src"
  javac -d build/classes $(find src/main/java -name \*.java)
  javadoc -d build/javadoc -sourcepath src/main/java -subpackages org
  jar -cvf "${_pkgname}.jar" -C build/classes .
}

package() {
  cd "$srcdir/${_pkgname}-$pkgver-src"

  # Install license file
  install -Dm644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r build/javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  jh mvn-install org.apache.commons ${_pkgname} $pkgver \
    pom.xml "${_pkgname}.jar" "${_pkgname}-$pkgver.jar"

  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"
}
