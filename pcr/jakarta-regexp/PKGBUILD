# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=jakarta-regexp
pkgver=1.5
pkgrel=1.parabola1
pkgdesc="Regular expression library for Java"
arch=('any')
license=('APACHE')
url="https://jakarta.apache.org/regexp/"
depends=('java-runtime')
makedepends=('apache-ant' 'jh')
source=("http://archive.apache.org/dist/jakarta/regexp/source/${pkgname}-${pkgver}.tar.gz"
        "http://repo.maven.apache.org/maven2/${pkgname}/${pkgname}/1.4/${pkgname}-1.4.pom")
md5sums=('b941b8f4de297827f3211c2cb34af199'
         'd7d0510793c9081445fad8903a423f90')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  rm -v "${pkgname}-${pkgver}.jar"
  rm -rv docs
  sed -i 's/1\.4/1\.5/g' build.xml
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ant jar javadocs
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "docs/api" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install ${pkgname} ${pkgname} ${pkgver} \
    "${srcdir}/${pkgname}-1.4.pom" \
    "build/${pkgname}-${pkgver}.jar" \
    "regexp.jar"

  ln -s "/usr/share/java/regexp.jar" \
    "${pkgdir}/usr/share/java/regexp-${pkgver}.jar"
}
