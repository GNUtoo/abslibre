# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_libname=xml-commons-external
pkgname=java-${_libname}
pkgver=1.4.01
pkgrel=1.parabola1
pkgdesc="Common code and guidelines for xml projects"
arch=('any')
url="http://xml.apache.org/commons/"
license=('APACHE')
depends=('java-runtime')
makedepends=('java-environment' 'jh')
source=("https://archive.apache.org/dist/xerces/xml-commons/source/${_libname}-${pkgver}-src.tar.gz"
        "https://repo1.maven.org/maven2/xml-apis/xml-apis/${pkgver}/xml-apis-${pkgver}.pom"
        "https://repo1.maven.org/maven2/xml-apis/xml-apis-ext/1.3.04/xml-apis-ext-1.3.04.pom")
md5sums=('2fea8e97a5d4d1a24bd05f5f62f3e04e'
         '56ff7e1240b55e9b2073f3f39ac4fab4'
         '83f69d3ba3c21b5a4a3991de4c945e64')

prepare() {
  cd ${srcdir}
  mkdir -p build/classes
  sed -i 's/1\.3\.04/1\.4\.01/g' xml-apis-ext-1.3.04.pom
}

build() {
  cd ${srcdir}
  javac -source 1.4 -d "build/classes" $(find org/ javax/ -name \*.java)
  javadoc -d "build/javadoc" javax.xml org.apache.xmlcommons org.w3c.css.sac org.w3c.dom

  cd "build/classes"
  jar -cvfm "../../xml-apis-ext.jar" "../../manifest.commons" \
    $(find -type d -name "sac" -o -name "smil" -o -name "svg")
  jar -cvfm "../../xml-apis.jar" "../../manifest.commons" \
    $(find -type f ! \( -wholename "*/sac/*" -o -wholename "*/smil/*" -o -wholename "*/svg/*" \))
}

package() {
  cd ${srcdir}

  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install xml-apis xml-apis ${pkgver} \
    "xml-apis-${pkgver}.pom" \
    "xml-apis.jar" \
    "xml-apis.jar"
  jh mvn-install xml-apis xml-apis-ext ${pkgver} \
    "xml-apis-ext-1.3.04.pom" \
    "xml-apis-ext.jar" \
    "xml-apis-ext.jar"

  ln -s "/usr/share/java/xml-apis.jar" \
    "${pkgdir}/usr/share/java/xml-apis-${pkgver}.jar"
  ln -s "/usr/share/java/xml-apis-ext.jar" \
    "${pkgdir}/usr/share/java/xml-apis-ext-${pkgver}.jar"
}
