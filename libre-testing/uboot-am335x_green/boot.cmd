setenv bootargs console=ttyO0,115200n8 root=/dev/mapper/internal-rootfs coherent_pool=1M
echo "Booting /boot/boot.scr"
ext4load mmc 1:1 $loadaddr /boot/vmlinuz-linux-libre
ext4load mmc 1:1 $rdaddr /boot/initramfs-linux-libre.img
set rdsize ${filesize}
ext4load mmc 1:1 $fdtaddr /boot/dtbs/linux-libre/am335x-bonegreen.dtb
bootz ${loadaddr} ${rdaddr}:${rdsize} ${fdtaddr}
