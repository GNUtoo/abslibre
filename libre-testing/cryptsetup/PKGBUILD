# Maintainer:  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Thomas Bächler <thomas@archlinux.org>

pkgname=cryptsetup
pkgver=2.0.4
pkgrel=1
pkgdesc='Userspace setup tool for transparent encryption of block devices using dm-crypt'
arch=(x86_64 ppc64le)
license=('GPL')
url='https://gitlab.com/cryptsetup/cryptsetup/'
groups=('base')
depends=('device-mapper' 'libgcrypt' 'popt' 'libutil-linux' 'json-c' 'argon2')
makedepends=('util-linux')
options=('!emptydirs')
validpgpkeys=('2A2918243FDE46648D0686F9D9B0577BD93E98FC') # Milan Broz <gmazyland@gmail.com>
source=("https://www.kernel.org/pub/linux/utils/cryptsetup/v2.0/${pkgname}-${pkgver}.tar."{xz,sign}
        'hooks-encrypt'
        'install-encrypt'
        'install-sd-encrypt')
sha256sums=('9d3a3c7033293e0c97f0ad0501fd5b4d4913ae497cbf70cca06633ccc54b5734'
            'SKIP'
            '416aa179ce3c6a7a5eee0861f1f0a4fafac91b69e84a2aae82b6e5a6140e31e2'
            '7b8c8a189f1b63cb4a0c0dd93d3452615bdc05f0e33570c78b338446a59ca750'
            '95a16baa273a0ea5c531bc4b65e7f142ae7cb6423b5e512f4413d1dd4a545421')

build() {
  cd "${srcdir}"/$pkgname-${pkgver}

  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --enable-libargon2 \
    --disable-static
  make
}

package() {
  cd "${srcdir}"/$pkgname-${pkgver}

  make DESTDIR="${pkgdir}" install

  # install hook
  install -D -m0644 "${srcdir}"/hooks-encrypt "${pkgdir}"/usr/lib/initcpio/hooks/encrypt
  install -D -m0644 "${srcdir}"/install-encrypt "${pkgdir}"/usr/lib/initcpio/install/encrypt
  install -D -m0644 "${srcdir}"/install-sd-encrypt "${pkgdir}"/usr/lib/initcpio/install/sd-encrypt
}
